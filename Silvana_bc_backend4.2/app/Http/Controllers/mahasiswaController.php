<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\mahasiswa;

class mahasiswaController extends Controller
{
    function getData(){

        $mahasiswa = mahasiswa::get();

        return response()->json($userList, 200);
    }

    function addData(Request $request){
        DB::beginTransaction();
        try{

            $this->validate($request, [
                'name' => 'required',
            ]);

            $nim = $request->input('nim');
            $mahasiswa_name = $request->input('mahasiswa_name');
            $mahasiswa_address = $request->input('mahasiswa_address');
            $mahasiswa_phonenumber = $request->input('mahasiswa_phonenumber');
            $mahasiswa_email = $request->input('mahasiswa_email');
            $mahasiswa_parents = $request->input('mahasiswa_parents');
            $parents_address = $request->input('parents_address');
            $parents_phonenumber = $request->input('parents_phonenumber');
            $high_school = $request->input('high_school');


            $mhsw = new mahasiswas;
            $mhsw->nim = $nim;
            $mhsw->mahasiswa_name = $mahasiswa_name;
            $mhsw->mahasiswa_address = $mahasiswa_address;
            $mhsw->mahasiswa_phonenumber = $mahasiswa_phonenumber;
            $mhsw->mahasiswa_email = $mahasiswa_email;
            $mhsw->mahasiswa_parents = $mahasiswa_parents;
            $mhsw->parents_address = $parents_address;
            $mhsw->parents_phonenumber = $parents_phonenumber;
            $mhsw->high_school = $high_school;
            $mhsw->save();

            DB::commit();
            return response()->json(["message" => "Success !!"], 200);
        }
        catch(\Exception $e){
            DB::rollBack();
            return response()->json(["message" => $e->getMessage()], 500);
        }

    }

    function saveData(Request $request){
        DB::beginTransaction();
        try{

            $this->validate($request, [
                'id' => 'required',
            ]);

            $id = (integer)$request->input('id');
            $mhsw = mahasiswa::find($id);

            if(failed($mhsw)){
                return response()->json(["message" => "failed"], 404);
            }
            $mhsw->save();

            DB::commit();
            return response()->json(["message" => "Success !!"], 200);

        }

        catch(\Exception $e){
            DB::rollBack();
            return response()->json(["message" => $e->getMessage()], 500);
        }

    }
}
