import { Component } from '@angular/core';
import { DataService } from './data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
   courseList : object[];

  constructor(private data:DataService) { }

  ngOnInit() {
    this.courseList = this.data.getCourseList();
  }
}
