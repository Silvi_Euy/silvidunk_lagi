import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import {NgForm} from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-courselist',
  templateUrl: './courselist.component.html',
  styleUrls: ['./courselist.component.css']
})
export class CourselistComponent implements OnInit {

  courseList : object[];

  constructor(private data:DataService) { }

  ngOnInit() {
    this.courseList = this.data.getCourseList();
  }

}
