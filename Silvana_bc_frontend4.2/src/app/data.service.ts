import { Injectable } from '@angular/core';

@Injectable()
export class DataService {

    private courseList : Object[] = [
    {"id":"1", "course_name":"a", "price":30000000, "tutorname":"AA"},
    {"id":"2", "course_name":"b", "price":50000000, "tutorname":"BB"},
    {"id":"3", "course_name":"c", "price":70000000, "tutorname":"CC"},
    {"id":"4", "course_name":"d", "price":80000000, "tutorname":"DD"}
    {"id":"5", "course_name":"e", "price":90000000, "tutorname":"EE"}
  ];

  constructor() { }

   getCourseList():object[]
  {
    return this.courseList;
  }

}
